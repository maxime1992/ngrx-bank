import { BankPage } from './app.po';

describe('bank App', () => {
  let page: BankPage;

  beforeEach(() => {
    page = new BankPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
