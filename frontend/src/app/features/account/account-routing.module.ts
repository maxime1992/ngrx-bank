import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from 'app/features/account/account.component';

// /bank
const routes: Routes = [
  {
    path: 'accounts/:accountId',
    component: AccountComponent,
    children: [
      {
        path: 'transfer',
        loadChildren:
          'app/features/account/transfer/transfer.module#TransferModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
