import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';
import { TransferComponent } from 'app/features/account/transfer/transfer.component';
import { TransferRoutingModule } from 'app/features/account/transfer/transfer-routing.module';

@NgModule({
  imports: [SharedModule, TransferRoutingModule],
  declarations: [TransferComponent],
})
export class TransferModule {}
