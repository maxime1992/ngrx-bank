import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss'],
})
export class TransferComponent implements OnInit {
  public transferFormGroup: FormGroup;
  public peopleWithAccounts: { title: string; accountId: string }[];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.transferFormGroup = this.fb.group({
      accountId: [null, [Validators.required]],
      amount: [null, [Validators.required]],
    });

    // as I don't have time to implement a user reducer and all,
    // we'll avoid all that for just a simple array (for now)
    this.peopleWithAccounts = [
      {
        personName: 'philippe',
        accounts: [
          {
            id: 'accountId2',
            name: 'current account',
          },
          {
            id: 'accountId3',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'lamarana',
        accounts: [
          {
            id: 'accountId6',
            name: 'current account',
          },
          {
            id: 'accountId7',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'thomas',
        accounts: [
          {
            id: 'accountId8',
            name: 'current account',
          },
          {
            id: 'accountId9',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'guillaume',
        accounts: [
          {
            id: 'accountId12',
            name: 'current account',
          },
          {
            id: 'accountId13',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'fabien',
        accounts: [
          {
            id: 'accountId10',
            name: 'current account',
          },
          {
            id: 'accountId11',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'sebastien',
        accounts: [
          {
            id: 'accountId4',
            name: 'current account',
          },
          {
            id: 'accountId5',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'anthony',
        accounts: [
          {
            id: 'accountId0',
            name: 'current account',
          },
          {
            id: 'accountId1',
            name: 'saving account',
          },
        ],
      },
      {
        personName: 'maxime',
        accounts: [
          {
            id: 'accountId14',
            name: 'current account',
          },
          {
            id: 'accountId15',
            name: 'saving account',
          },
        ],
      },
    ].reduce(flatten, []);
  }

  transfer() {
    // TODO
    // this should dispatch an action
    // that'd be catched by an effect to trigger the HTTP post
    console.log(this.transferFormGroup.value);
  }
}

// flatten the array and concat the name with an account
function flatten(acc, x) {
  return [
    ...acc,
    ...x.accounts.map(account => ({
      title: `${x.personName} | ${account.name}`,
      accountId: account.id,
    })),
  ];
}
