import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferComponent } from 'app/features/account/transfer/transfer.component';

// /bank/accounts/:accountId/transfer
const routes: Routes = [
  {
    path: '',
    component: TransferComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransferRoutingModule {}
