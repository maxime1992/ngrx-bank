import { NgModule } from '@angular/core';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from 'app/features/account/account.component';
import { OperationsComponent } from 'app/features/account/operations/operations.component';
import { OperationComponent } from 'app/features/account/operations/operation/operation.component';
import { AccountSummaryComponent } from 'app/features/account/account-summary/account-summary.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [AccountRoutingModule, SharedModule],
  declarations: [
    AccountComponent,
    OperationsComponent,
    OperationComponent,
    AccountSummaryComponent,
  ],
})
export class AccountModule {}
