import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { IStore } from 'app/shared/interfaces/store.interface';
import { FetchOperations } from 'app/shared/states/operations/operations.actions';
import { IOperationBackendWithoutDetails } from 'app/shared/states/operations/operations.interfaces';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  operations$: Observable<IOperationBackendWithoutDetails[]>;

  constructor(private route: ActivatedRoute, private store$: Store<IStore>) {}

  ngOnInit() {
    this.route.params
      .map(params => params['accountId'])
      .do(accountId => this.store$.dispatch(new FetchOperations({ accountId })))
      .subscribe();

    this.operations$ = this.store$.select(state =>
      state.operations.allIds.map(
        operationId => state.operations.byId[operationId]
      )
    );
  }
}
