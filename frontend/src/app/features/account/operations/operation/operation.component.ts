import { Component, OnInit, Input } from '@angular/core';
import { IOperationBackendWithoutDetails } from 'app/shared/states/operations/operations.interfaces';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.scss'],
})
export class OperationComponent implements OnInit {
  @Input() operation: IOperationBackendWithoutDetails;

  constructor() {}

  ngOnInit() {}
}
