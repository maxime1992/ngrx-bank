import { Component, OnInit, Input } from '@angular/core';

import { IOperationBackendWithoutDetails } from 'app/shared/states/operations/operations.interfaces';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.scss'],
})
export class OperationsComponent implements OnInit {
  @Input() operations: IOperationBackendWithoutDetails[];

  constructor() {}

  ngOnInit() {}
}
