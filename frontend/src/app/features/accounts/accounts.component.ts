import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IStore } from 'app/shared/interfaces/store.interface';
import * as AccountsActions from 'app/shared/states/accounts/accounts.actions';
import { IAccountBackendWithoutDetails } from 'app/shared/states/accounts/accounts.interfaces';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {
  accounts$: Observable<IAccountBackendWithoutDetails[]>;

  constructor(private store$: Store<IStore>) {}

  ngOnInit() {
    this.store$.dispatch(new AccountsActions.FetchAccounts());

    this.accounts$ = this.store$.select(state =>
      state.accounts.allIds.map(accountId => state.accounts.byId[accountId])
    );
  }
}
