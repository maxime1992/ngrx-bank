import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { AccountsComponent } from './accounts/accounts.component';
import { AccountComponent } from './account/account.component';
import { AccountSummaryComponent } from './account/account-summary/account-summary.component';
import { OperationsComponent } from './account/operations/operations.component';
import { OperationComponent } from './account/operations/operation/operation.component';

@NgModule({
  imports: [SharedModule, FeaturesRoutingModule],
  declarations: [FeaturesComponent, AccountsComponent],
})
export class FeaturesModule {}
