/**
 *
 * @template T The type of one entry for the byId attribute
 * @param {T[]} res The array to convert into a table
 * @param {string} [uniqueId='id'] If type T doesn't have an 'id' key, specify a unique key to use instead
 * @returns {{ byId: { [key: string]: T }; allIds: string[] }}
 *
 */
export function arrayToTable<T>(
  res: T[],
  uniqueId = 'id'
): { byId: { [key: string]: T }; allIds: string[] } {
  return res.reduce(
    (acc, curr) => {
      acc.byId[curr[uniqueId]] = curr;
      acc.allIds.push(curr[uniqueId]);

      return acc;
    },
    { byId: {}, allIds: [] }
  );
}
