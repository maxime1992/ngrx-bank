import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import { IStore } from 'app/shared/interfaces/store.interface';
import * as OperationsActions from './operations.actions';
import { environment } from 'environments/environment';
import { AccountsService } from 'app/shared/states/accounts/accounts.service';
import { OperationsService } from 'app/shared/states/operations/operations.service';

@Injectable()
export class OperationsEffects {
  constructor(
    private store$: Store<IStore>,
    private actions$: Actions,
    private operationsService: OperationsService
  ) {}

  // tslint:disable-next-line:member-ordering
  @Effect({ dispatch: true })
  fetchOperations$: Observable<Action> = this.actions$
    .ofType(OperationsActions.FETCH_OPERATIONS)
    .switchMap((action: OperationsActions.FetchOperations) =>
      this.operationsService
        .fetchOperations(action.payload.accountId)
        .map(
          operationsTable =>
            new OperationsActions.FetchOperationsSuccess({
              accountId: action.payload.accountId,
              operationsTable,
            })
        )
        .catch((err: Response) => {
          if (environment.debug) {
            console.group();
            console.warn('Error caught in accounts.effects:');
            console.error(err);
            console.groupEnd();
          }

          return Observable.of(
            new OperationsActions.FetchOperationsFailed({
              accountId: action.payload.accountId,
              error: err.toString(),
            })
          );
        })
    );
}
