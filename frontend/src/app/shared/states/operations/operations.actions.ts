import { Action } from '@ngrx/store';
import { IOperationsTable } from 'app/shared/states/operations/operations.interfaces';

/**
 * create one class per action and do not forget to add them at the end to the type All
 * this way, you'll be able to have type checking when dispatching and also in your reducers
 */
export const FETCH_OPERATIONS = 'Fetch operations';
export class FetchOperations implements Action {
  readonly type = FETCH_OPERATIONS;

  constructor(public payload: { accountId: string }) {}
}

export const FETCH_OPERATIONS_SUCCESS = 'Fetch operations success';
export class FetchOperationsSuccess implements Action {
  readonly type = FETCH_OPERATIONS_SUCCESS;

  constructor(
    public payload: { accountId: string; operationsTable: IOperationsTable }
  ) {}
}

export const FETCH_OPERATIONS_FAILED = 'Fetch operations failed';
export class FetchOperationsFailed implements Action {
  readonly type = FETCH_OPERATIONS_FAILED;

  constructor(public payload: { accountId: string; error: string }) {}
}

// list every action class here
export type All =
  | FetchOperations
  | FetchOperationsSuccess
  | FetchOperationsFailed;
