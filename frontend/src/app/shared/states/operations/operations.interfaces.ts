// ------------------
// FOR A SINGLE OPERATION
// ------------------
// definition of a "minimal" Operation as it is on the backend
export interface IOperationBackendWithoutDetails {
  readonly id: string;
  readonly name: string;
  readonly date: Date;
  readonly amount: number;
}

// definition of a "detailed" Operation as it is on the backend
// without foreign keys
export interface IOperationBackendWithDetails extends IOperationBackendWithoutDetails {}

// definition of a "detailed" Operation as it is on the backend with foreign keys
export interface IOperationBackendWithDetailsAndFk extends IOperationBackendWithDetails {}

// definition of everything related to an Operation **and** UI
interface IOperationUi {}

// definition of everything related to a minimal Operation **and** UI
// tslint:disable-next-line:no-empty-interface
export interface IOperationBackendWithoutDetailsUi extends IOperationBackendWithoutDetails, IOperationUi {}

// definition of everything related to a detailed Operation **and** UI
// tslint:disable-next-line:no-empty-interface
export interface IOperationBackendWithDetailsAndFkUi extends IOperationBackendWithDetailsAndFk, IOperationUi {}

// ----------------------------------------------------------------------------

// -------------------
// FOR MULTIPLE OPERATIONS
// -------------------
// definition of the UI attributes you want to save for all the operations
interface IOperationsUi {}

// definition of the Operations with byId and allIds for state normalization
export interface IOperationsTable extends IOperationsUi {
  readonly byId: { [key: string]: IOperationBackendWithDetailsAndFkUi };
  readonly allIds: string[];
}
