import { IPizzasTable } from 'app/shared/states/pizzas/pizzas.interfaces';
import { pizzasInitialState } from 'app/shared/states/pizzas/pizzas.initial-state';
import * as OperationsActions from './operations.actions';
import { IAccountsTable } from 'app/shared/states/accounts/accounts.interfaces';
import { IOperationsTable } from 'app/shared/states/operations/operations.interfaces';
import { operationsInitialState } from 'app/shared/states/operations/operations.initial-state';

export function operationsReducer(
  operationsTable: IOperationsTable = operationsInitialState(),
  action: OperationsActions.All
): IOperationsTable {
  switch (action.type) {
    case OperationsActions.FETCH_OPERATIONS: {
      // here, notice that the action.payload is typed accordingly to this action
      return {
        ...operationsTable,
        isFetchingAccounts: true,
        isFetchingAccountsError: '',
      };
    }

    case OperationsActions.FETCH_OPERATIONS_SUCCESS: {
      return {
        ...operationsTable,
        ...action.payload.operationsTable,
        isFetchingAccounts: false,
        isFetchingAccountsError: '',
      };
    }

    case OperationsActions.FETCH_OPERATIONS_FAILED: {
      return {
        ...operationsTable,
        isFetchingAccounts: false,
        isFetchingAccountsError: action.payload.error,
      };
    }

    default:
      return operationsTable;
  }
}
