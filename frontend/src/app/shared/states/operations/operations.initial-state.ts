import {
  IAccountBackendWithDetailsAndFk,
  IAccountBackendWithDetailsAndFkUi,
  IAccountsTable,
} from 'app/shared/states/accounts/accounts.interfaces';
import {
  IOperationBackendWithDetailsAndFk,
  IOperationBackendWithDetailsAndFkUi,
  IOperationsTable,
} from 'app/shared/states/operations/operations.interfaces';

/**
 * pass an operation and return an operation with its properties + missing ones
 * this function might be helpful to initialize operations coming from the server
 */
export function operationInitialState(
  operation: IOperationBackendWithDetailsAndFk
): IOperationBackendWithDetailsAndFkUi {
  const emptyObj: IOperationBackendWithDetailsAndFkUi = {
    id: '',
    name: '',
    date: new Date(),
    amount: 0,
  };

  return { ...emptyObj, ...operation };
}

/**
 * default state for IOperationsTable
 */
export function operationsInitialState(): IOperationsTable {
  return {
    byId: {},
    allIds: [],
  };
}
