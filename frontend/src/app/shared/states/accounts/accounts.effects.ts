import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import { IStore } from 'app/shared/interfaces/store.interface';
import * as AccountsActions from './accounts.actions';
import { environment } from 'environments/environment';
import { AccountsService } from 'app/shared/states/accounts/accounts.service';

@Injectable()
export class AccountsEffects {
  constructor(
    private store$: Store<IStore>,
    private actions$: Actions,
    private accountsService: AccountsService
  ) {}

  // tslint:disable-next-line:member-ordering
  @Effect({ dispatch: true })
  fetchAccounts$: Observable<Action> = this.actions$
    .ofType(AccountsActions.FETCH_ACCOUNTS)
    .switchMap((action: AccountsActions.FetchAccounts) =>
      this.accountsService
        .fetchAccounts()
        .map(
          accountsTable =>
            new AccountsActions.FetchAccountsSuccess(accountsTable)
        )
        .catch((err: Response) => {
          if (environment.debug) {
            console.group();
            console.warn('Error caught in accounts.effects:');
            console.error(err);
            console.groupEnd();
          }

          return Observable.of(
            new AccountsActions.FetchAccountsFailed({
              error: err.toString(),
            })
          );
        })
    );
}
