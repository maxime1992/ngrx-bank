import { Action } from '@ngrx/store';
import { IAccountsTable } from 'app/shared/states/accounts/accounts.interfaces';

/**
 * create one class per action and do not forget to add them at the end to the type All
 * this way, you'll be able to have type checking when dispatching and also in your reducers
 */
export const FETCH_ACCOUNTS = 'Fetch accounts';
export class FetchAccounts implements Action {
  readonly type = FETCH_ACCOUNTS;

  constructor() {}
}

export const FETCH_ACCOUNTS_SUCCESS = 'Fetch accounts success';
export class FetchAccountsSuccess implements Action {
  readonly type = FETCH_ACCOUNTS_SUCCESS;

  constructor(public payload: IAccountsTable) {}
}

export const FETCH_ACCOUNTS_FAILED = 'Fetch accounts failed';
export class FetchAccountsFailed implements Action {
  readonly type = FETCH_ACCOUNTS_FAILED;

  constructor(public payload: { error: string }) {}
}

// list every action class here
export type All = FetchAccounts | FetchAccountsSuccess | FetchAccountsFailed;
