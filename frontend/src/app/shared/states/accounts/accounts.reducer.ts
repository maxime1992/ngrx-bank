import { IPizzasTable } from 'app/shared/states/pizzas/pizzas.interfaces';
import { pizzasInitialState } from 'app/shared/states/pizzas/pizzas.initial-state';
import * as AccountsActions from './accounts.actions';
import * as OperationsActions from '../operations/operations.actions';
import { IAccountsTable } from 'app/shared/states/accounts/accounts.interfaces';
import { accountsInitialState } from 'app/shared/states/accounts/accounts.initial-state';

export function accountsReducer(
  accountsTable: IAccountsTable = accountsInitialState(),
  action: AccountsActions.All | OperationsActions.All
): IAccountsTable {
  switch (action.type) {
    case AccountsActions.FETCH_ACCOUNTS: {
      // here, notice that the action.payload is typed accordingly to this action
      return {
        ...accountsTable,
        isFetchingAccounts: true,
        isFetchingAccountsError: '',
      };
    }

    case AccountsActions.FETCH_ACCOUNTS_SUCCESS: {
      return {
        ...accountsTable,
        ...action.payload,
        isFetchingAccounts: false,
        isFetchingAccountsError: '',
      };
    }

    case AccountsActions.FETCH_ACCOUNTS_FAILED: {
      return {
        ...accountsTable,
        isFetchingAccounts: false,
        isFetchingAccountsError: action.payload.error,
      };
    }

    // HANDLE OPERATIONS
    case OperationsActions.FETCH_OPERATIONS: {
      // here, notice that the action.payload is typed accordingly to this action
      return {
        ...accountsTable,
        byId: {
          ...accountsTable.byId,
          [action.payload.accountId]: {
            ...accountsTable.byId[action.payload.accountId],
            isFetchingOperations: true,
            isFetchingOperationsError: '',
          },
        },
      };
    }

    case OperationsActions.FETCH_OPERATIONS_SUCCESS: {
      return {
        ...accountsTable,
        byId: {
          ...accountsTable.byId,
          [action.payload.accountId]: {
            ...accountsTable.byId[action.payload.accountId],
            isFetchingOperations: false,
            isFetchingOperationsError: '',
          },
        },
      };
    }

    case OperationsActions.FETCH_OPERATIONS_FAILED: {
      return {
        ...accountsTable,
        byId: {
          ...accountsTable.byId,
          [action.payload.accountId]: {
            ...accountsTable.byId[action.payload.accountId],
            isFetchingOperations: false,
            isFetchingOperationsError: action.payload.error,
          },
        },
      };
    }

    default:
      return accountsTable;
  }
}
