import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import {
  IAccountsTable,
  IAccountBackendWithDetailsAndFk,
} from 'app/shared/states/accounts/accounts.interfaces';
import { accountInitialState } from 'app/shared/states/accounts/accounts.initial-state';
import { arrayToTable } from 'app/shared/helpers/http.helper';

// when creating a service, you should use an abstract class to describe your methods
// this way you'll have the possibility to :
// - make sure you've got the same parameters and return types between the real service and the mock
// - search for references in Visual Studio Code and find your mock aswell on a method
@Injectable()
export abstract class AccountsService {
  abstract fetchAccounts(): Observable<IAccountsTable>;
}

@Injectable()
export class AccountsServiceImpl extends AccountsService {
  constructor(private http: Http) {
    super();
  }

  fetchAccounts(): Observable<IAccountsTable> {
    return this.http
      .get(`${environment.urlBackend}/accounts?userId=maxime`)
      .map((res: Response) => res.json() as IAccountBackendWithDetailsAndFk[])
      .map(accounts => accounts.map(account => accountInitialState(account)))
      .map(accounts => arrayToTable(accounts));
  }
}
