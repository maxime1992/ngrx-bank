// ------------------
// FOR A SINGLE ACCOUNT
// ------------------
// definition of a "minimal" Account as it is on the backend
export interface IAccountBackendWithoutDetails {
  readonly id: string;
  readonly name: string;
}

// definition of a "detailed" Account as it is on the backend
// without foreign keys
export interface IAccountBackendWithDetails extends IAccountBackendWithoutDetails {}

// definition of a "detailed" Account as it is on the backend with foreign keys
export interface IAccountBackendWithDetailsAndFk extends IAccountBackendWithDetails {
  readonly operationsIds: string[];
}

// definition of everything related to an Account **and** UI
interface IAccountUi {
  readonly isFetchingOperations: boolean;
  readonly isFetchingOperationsError: string;
}

// definition of everything related to a minimal Account **and** UI
// tslint:disable-next-line:no-empty-interface
export interface IAccountBackendWithoutDetailsUi extends IAccountBackendWithoutDetails, IAccountUi {}

// definition of everything related to a detailed Account **and** UI
// tslint:disable-next-line:no-empty-interface
export interface IAccountBackendWithDetailsAndFkUi extends IAccountBackendWithDetailsAndFk, IAccountUi {}

// ----------------------------------------------------------------------------

// -------------------
// FOR MULTIPLE ACCOUNTS
// -------------------
// definition of the UI attributes you want to save for all the accounts
interface IAccountsUi {
  isFetchingAccounts: boolean;
  isFetchingAccountsError: string;
}

// definition of the Account with byId and allIds for state normalization
export interface IAccountsTable extends IAccountsUi {
  readonly byId: { [key: string]: IAccountBackendWithDetailsAndFkUi };
  readonly allIds: string[];
}
