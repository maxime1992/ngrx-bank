import {
  IAccountBackendWithDetailsAndFk,
  IAccountBackendWithDetailsAndFkUi,
  IAccountsTable,
} from 'app/shared/states/accounts/accounts.interfaces';

/**
 * pass an account and return an account with its properties + missing ones
 * this function might be helpful to initialize accounts coming from the server
 */
export function accountInitialState(
  account: IAccountBackendWithDetailsAndFk
): IAccountBackendWithDetailsAndFkUi {
  const emptyObj: IAccountBackendWithDetailsAndFkUi = {
    id: '',
    name: '',
    operationsIds: [],
    isFetchingOperations: false,
    isFetchingOperationsError: '',
  };

  return { ...emptyObj, ...account };
}

/**
 * default state for IAccountsTable
 */
export function accountsInitialState(): IAccountsTable {
  return {
    isFetchingAccounts: false,
    isFetchingAccountsError: '',

    byId: {},
    allIds: [],
  };
}
