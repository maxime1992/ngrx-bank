import { IUi } from '../states/ui/ui.interface';
import { IAccountsTable } from 'app/shared/states/accounts/accounts.interfaces';
import { IOperationsTable } from 'app/shared/states/operations/operations.interfaces';

export interface IStore {
  readonly ui: IUi;
  readonly accounts: IAccountsTable;
  readonly operations: IOperationsTable;
}
