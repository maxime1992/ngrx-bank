const { usersInst, User } = require('./users');
const { accountsInst, Account } = require('./accounts');
const { operationsInst, Operation } = require('./operations');

const createUserWithAccountsAndOperations = username => {
  const operationsAccount1 = [
    new Operation('some operation acc 1', new Date(), 15.21),
    new Operation('some operation acc 1', new Date(), 30.72),
  ];

  const operationsAccount2 = [
    new Operation('some operation acc 2', new Date(), 85.39),
    new Operation('some operation acc 2', new Date(), 30.92),
    new Operation('some operation acc 2', new Date(), 18.14),
  ];

  operationsAccount1.forEach(operation =>
    operationsInst.addOperation(operation)
  );
  operationsAccount2.forEach(operation =>
    operationsInst.addOperation(operation)
  );

  const accounts = [
    new Account(
      'current account',
      operationsAccount1.map(operation => operation.id)
    ),
    new Account(
      'saving account',
      operationsAccount2.map(operation => operation.id)
    ),
  ];

  accounts.forEach(account => accountsInst.addAccount(account));

  const users = [new User(username, accounts.map(account => account.id))];

  users.map(user => usersInst.addUser(user));
};

const users = [
  'anthony',
  'philippe',
  'sebastien',
  'lamarana',
  'thomas',
  'fabien',
  'guillaume',
  'maxime',
];

// create users with accounts, and operations
users.map(createUserWithAccountsAndOperations);
