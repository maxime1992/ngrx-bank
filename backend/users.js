class Users {
  constructor() {
    this.cpt = 0;

    this.byId = {};

    this.allIds = [];
  }

  getNewId() {
    return this.cpt++;
  }

  addUser(user) {
    this.byId = Object.assign({}, this.byId, { [user.id]: user });
    this.allIds = [user.id, ...this.allIds];
  }

  getListUsers() {
    return this.allIds.map(userId => this.byId[userId]);
  }

  getUser(userId) {
    return this.byId[userId];
  }
}

const usersInst = new Users();

class User {
  constructor(name, accountsIds) {
    // as we want to fake a token to skip the connection step,
    // we would have to pass an ID that wouldn't not help us identify
    // for which personalbar. Instead of that use the name as ID (and token)
    // this.id = `userId${usersInst.getNewId()}`;
    this.id = name;
    this.name = name;
    this.accountsIds = accountsIds;
  }

  addAccount(accountId) {
    this.accountsIds = [accountId, ...this.accountsIds];
  }

  getAccountsIds() {
    return this.accountsIds;
  }
}

module.exports = { usersInst, User };
