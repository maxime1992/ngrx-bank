const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const { accountsInst } = require('./accounts');
const { operationsInst, Operation } = require('./operations');
const { usersInst } = require('./users');
const usersData = require('./users.data');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const cors = require('cors');
app.use(cors());

app.get('/accounts', (req, res) => {
  const userId = req.query.userId;
  const user = usersInst.getUser(userId);

  const userAccounts = user
    .getAccountsIds()
    .map(accountId => accountsInst.getAccount(accountId));

  res.json(userAccounts);
});

app.get('/accounts/:accountId/operations', (req, res) => {
  const userId = req.query.userId;
  const user = usersInst.getUser(userId);
  const userAccounts = user.getAccountsIds();

  const { accountId } = req.params;

  if (!userAccounts.includes(accountId)) {
    return res.status(404).send('Not found');
  }

  const operationsIds = accountsInst.getAccount(accountId).getOperationsIds();
  const operations = operationsIds.map(operationId =>
    operationsInst.getOperation(operationId)
  );

  res.json(operations);
});

// a transfer should have in post data
// { toAccountId: 'someAccountId', name: 'Some name for the transfer', date: ..., amount: 50.45 }
app.post('/accounts/:accountId/transfer', (req, res) => {
  const userId = req.query.userId;
  const user = usersInst.getUser(userId);
  const userAccounts = user.getAccountsIds();

  const { accountId } = req.params;

  if (!userAccounts.includes(accountId)) {
    return res.status(404).send('Not found');
  }

  // get data from the post
  let { toAccountId, name, date, amount } = (transfer = req.body);

  // let's make sure that we have a positive number
  amount = amount > 0 ? amount : -amount;

  // create and save the operation: debit
  const operationFrom = new Operation(name, date, -amount);
  operationsInst.addOperation(operationFrom);
  accountsInst.getAccount(accountId).addOperation(operationFrom.getId());

  // create and save the operation: credit
  const operationTo = new Operation(name, date, amount);
  operationsInst.addOperation(operationTo);
  accountsInst.getAccount(toAccountId).addOperation(operationTo.getId());

  res.json(operationFrom);
});

app.listen(3000, () => {
  console.log('Server listening on 3000');
});
