class Accounts {
  constructor() {
    this.cpt = 0;

    this.byId = {};

    this.allIds = [];
  }

  getNewId() {
    return this.cpt++;
  }

  addAccount(account) {
    this.byId = Object.assign({}, this.byId, { [account.id]: account });
    this.allIds = [account.id, ...this.allIds];
  }

  getListAccounts() {
    return this.allIds.map(accountId => this.byId[accountId]);
  }

  getAccount(accountId) {
    return this.byId[accountId];
  }
}

const accountsInst = new Accounts();

class Account {
  constructor(name, operationsIds) {
    this.id = `accountId${accountsInst.getNewId()}`;
    this.name = name;
    this.operationsIds = operationsIds;
  }

  addOperation(operationId) {
    this.operationsIds = [operationId, ...this.operationsIds];
  }

  getOperationsIds() {
    return this.operationsIds;
  }
}

module.exports = { accountsInst, Account };
