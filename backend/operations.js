class Operations {
  constructor() {
    this.cpt = 0;

    this.byId = {};

    this.allIds = [];
  }

  getNewId() {
    return this.cpt++;
  }

  addOperation(operation) {
    this.byId = Object.assign({}, this.byId, { [operation.id]: operation });
    this.allIds = [operation.id, ...this.allIds];
  }

  getOperation(operationId) {
    return this.byId[operationId];
  }
}

const operationsInst = new Operations();

class Operation {
  constructor(name, date, amount) {
    this.id = `operationId${operationsInst.getNewId()}`;
    this.name = name;
    this.date = date;
    this.amount = amount;
  }

  getId() {
    return this.id;
  }
}

module.exports = { operationsInst, Operation };
